package main

import (
	"bufio"
	"day_1/pkg/stack"
	"day_1/pkg/utils"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatalf("Unable to open file")
	}

	scanner := bufio.NewScanner(file)

	var tmp []int
	s := stack.NewStack()
	for scanner.Scan() {
		line := scanner.Text()
		i, err := strconv.Atoi(strings.TrimSpace(line))

		if err != nil {
			s.Add(utils.AddSlice(tmp))
			tmp = nil
		} else {
			tmp = append(tmp, i)
		}

	}

	print(utils.AddSlice(s.Stack))
}
