package utils

import "testing"

func TestAddSlice(t *testing.T) {
  result := AddSlice([]int{1,2,3,4,5,6})
  if result != 21{
    t.Errorf("Expected: %d; Got: %d",21,result)
  }
}
