package stack

import (
	"fmt"
	"sort"
)

type Stack struct {
	Stack []int
}

func NewStack() *Stack {
	return &Stack{}
}

func (s *Stack) Add(item int) {
	if len(s.Stack) >= 3 {

		sort.Slice(s.Stack, func(i, j int) bool {
			return s.Stack[i] > s.Stack[j]
		})

		if item > s.Stack[2] {
			s.Stack[2] = item
		}
	} else {
		s.Stack = append(s.Stack, item)
	}
}

func (s *Stack) Print() {
	fmt.Println(s.Stack)
}
