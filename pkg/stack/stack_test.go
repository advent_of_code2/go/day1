package stack

import (
	"testing"
)

func contains(a []int, b int) bool {
	for _, v := range a {
		if v == b {
			return true
		}
	}
	return false
}

func TestAdd(t *testing.T) {
	s := NewStack()
	s.Add(1)
	s.Add(2)
	s.Add(3)
	s.Add(0)
	s.Add(5)

	tests := []struct {
		value    int
		expected bool
	}{
		{1, false},
		{2, true},
		{3, true},
		{0, false},
		{5, true},
	}

	for _, test := range tests {
		got := contains(s.Stack, test.value)
		if got != test.expected {
			t.Errorf("Value: %d is %v but got %v", test.value, test.expected, got)
		}

	}

}
